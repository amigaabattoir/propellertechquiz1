#Propeller Tech Quiz 1
Search box, with autocomplete to match finding users by their profile name. Either show the user's info or an error message.

## Hosted Link
Hosted link [http://aws-website-propellertechquiz-k6tml.s3-website-us-east-1.amazonaws.com/](http://aws-website-propellertechquiz-k6tml.s3-website-us-east-1.amazonaws.com/)

## References
[Bootstrap 3](http://getbootstrap.com/)

Typeahead from [Bootstrap-3-Typeahead](https://github.com/bassjobsen/Bootstrap-3-Typeahead)